var app     = require('express')();
var http    = require('http').Server(app);
var io      = require('socket.io')(http);

users       = [];
connections = [];

http.listen(3000, function(){
  console.log('listening on *:3000');
});

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


io.on('connection', function(socket){
  connections.push(socket);
  console.log('Connected: %s', connections.length);

  //disconnect
  socket.on('disconnect', function(data){
  	//update users
  	users.splice(users.indexOf(socket.username), 1);
  	updateUserNames();
  	//update connections
  	connections.splice(connections.indexOf(socket), 1)
  	console.log('Disconnected: %s', connections.length);
  })

  //send message
  socket.on('send message', function(data){
  	console.log(data);
  	io.sockets.emit('new message', {msg: data, user: socket.username});
  });

  //new user
  socket.on('new user', function(data, callback){
  	callback(true);
  	socket.username = data;
  	users.push(socket.username);
  	updateUserNames();
  });

  function updateUserNames(){
  	io.sockets.emit('get users', users);
  }
});

